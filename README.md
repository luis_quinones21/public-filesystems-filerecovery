
[English](#markdown-header-file-system-file-recovery) | [Español](#markdown-header-systema-de-archivos-recuperacion-de-archivos)

# Sistema de Archivos - Recuperación de Archivos
##Objetivos

1. Entender la implementación de los inodes, bloques de datos y otros conceptos de sistemas de archivos a través de la recuperación de archivos borrados utilizando la herramienta sleuthkit.

### Pre-Modulo

Antes de trabajar en este módulo, el estudiante debe:

1. Haber repasado conceptos de sistemas de archivos como inode, atributos de los archivos y bloques de datos.
2. Tener disponible una máquina virtual o una computadora con el sleuthkit instalado.
    - http://atackpr.ccom.uprrp.edu/vms/sleuthkit.tar.gz

## Aprendiendo conceptos de sistemas de archivos a través de la recuperación de un archivo borrado 

Normalmente cuando un archivo es removido del sistema de archivos, los datos de el archivo se mantienen en los bloques de datos del sistema de archivos hasta que estos son reclamados por otro archivo.

Conocer cómo los archivos están organizados en un sistema de archivos es esencial para la informática forense porque esto permite recuperar archivos que han sido borrados o escondidos dentro del sistema de archivos.

Como debió aprender en el curso, la mayoría de los sistemas de archivos tienen una estructura jerárquica organizada como un árbol de arriba hacia abajo donde las hojas de los árboles son los archivos y los directorios vacíos, y los nodos que forman el camino a las hojas son directorios.  

![](images/fs_tree.png)

Específicamente el sistema de archivos Unix, es una colección de archivos y directorios que tienen las siguiente propiedades:

* Un directorio (/) raíz 
* Cada archivo y directorio tiene un nombre y un identificador único, típicamente llamado el inode.
* El directorio raíz tiene el número de inode 2.  Los números 0 y 1 no están asignados a los inodes.

El medio de almacenamiento está lógicamente divido en bloques. Los bloques son divididos en bloques de datos y en los inodes.

Los bloques de datos contienen los datos dentro de los archivos.

Hay dos tipo de inodes:

* Inodes de directorio que contienen los nombres de los archivos y sub directorios dentro del directorio y un apuntador (número de inode) al inode del archivo o directorio.
    
    ![](images/directory_inode.png)

* Inodes de archivo que contienen los atributos de los archivos, y apuntadores a los bloques de datos del archivo.

    ![](images/file_inode.png)

Cuando un usuario remueve un archivo, los bloques de datos del archivo están listados como bloques libres, y el inode del archivo como no localizado. Entonces si no han sido escritos por encima los datos de los archivos están todavía presentes.

Para recuperar archivos borrados podemos buscar a través de los inodes no localizados, y de la información del inode obtener los bloques de datos que contienen los datos del archivo.

En este laboratorio aprenderemos cómo recuperar archivos utilizando el sleuthkit[1] que tiene comandos que permiten a los usuarios interactuar con los inodes del sistema de archivos.  Estaremos usando una imagen de una partición ext2 de Unix creados con el comando `dd`.  El nombre de la imagen es `recover_fs.dd`.

Los comandos principales que serán usados en este laboratorio son:

* `file` - Te permite conocer el tipo de sistema de archivos de una imagen. Una alternativa es usar `fstsat` que provee información más detallada sobre los grupos de inodes, tamaño de bloques, etc.

* `fls` - Lista los nombres de archivos y directorios en una imagen de disco.

    Usaremos fls  <image> <inode>, si no se provee un inode, el utilizará el inode raíz por defecto.  Para ext2 y ext3 el número del inode raíz es 2.

* `ils` - Lista la información de un inode

* `istat` - Despliega detalles del inode (más fácil de leer que ils)

* `icat` - Enseña el contenido de un archivo basado en su número de inode.

**Nota** Las imágenes y archivos en el sistema de archivos usados en este laboratorio están listados en la referencias.

**Note** Hay otras herramientas que facilitan la recuperación de archivos como autopsy que es una interface web para el sleuthkit, foremost, y Test-Disk. 

### Listando directorios 

```
fls recover_fs.dd
```

o 

```
fls recover_fs.dd 2
```

![](images/fls_2.png)

Nota los tres directorios y presta atención a los directorios `Documents` inode 16065 y `images` 46185

```
fls recover_fs.dd 16065
```

![](images/fls_16065.png)

Nota los dos archivos pdf, uno tiene * el otro no. También nota que el número de inode es 0.

```
fls recover_fs.dd 46185
```

![](images/fls_46185.png)

Nota todos los archivos con `*` (borrados), el directorio `jokes` con inode 36722.

### Obteniendo la información del inode

Vamos a obtener la información del archivo borrado en el directorio  `Documents`, archivo `rooter.pdf`

Corre 

```
ils recover_fs.dd -A 
```

para listar todos los inodes no localizados.

![](images/ils_A.png)

Recuerda que el número de inode de el directorio `Documents` es 16065, y que el número de inode más cercano (primer campo de las entradas) en los inodes no localizados es 16067.

### Mirando dentro del inode de un archivo

Corre 

```
ils recover_fs.dd 16067
```

para ver los atributos del inode, tales como id del usuario y grupo, y los tiempos de Acceso, Moficado, y Borrado.

![](images/istat_16067.png)

y la segunda parte del inode `Direct blocks` y `Indirect Blocks`

![](images/istat_16067_2.png)

Nota que el primer bloque directo (Direct block) se encuentra en el bloque de datos 70657, y el último bloque directo es 79749, y que también apunta a un bloque indirecto 70679 que implica que el archivo es lo suficientemente grande para necesitar tener un bloque indirecto con dirección para otros bloques directos.

### Recuperando el archivo

```
icat recover_fs.dd 16067 > recover1
```

Va a obtener los datos de los bloques de datos del archivo referenciado por los bloques directos e indirectos en el inode y los almacena en nuestro sistema de archivos con el nombre recover1.

Corriendo

```
file recover1
```

revelará el tipo del archivo.

## Ejercicio 1: Recuperando un archivo

1. ¿Cuales son los atributos de tiempo para el inode 46186?
2. ¿Cuales son el primer y el último bloque directo?
3. ¿Tiene bloques indirectos? ¿Cuál?
4. Recupera el archivo en el inode 46186.
5. ¿Qué tipo de archivo recuperaste?

## Ejercicio 2: Recuperando todos los archivos borrados

Recupera todos los archivos restantes con inodes no localizados.

## Referencias

[1] http://www.sleuthkit.org/sleuthkit/

[2] SCIgen - An Automatic CS Paper Generator, http://pdos.csail.mit.edu/scigen/

[3] http://fc09.deviantart.net/fs70/f/2014/079/1/9/sonic\_the\_hedgehog\_2006\_pose\_by\_mintenndo-d7axbgr.png

[4] http://engineering.vanderbilt.edu/images/dept-slide-show/eecs-slide-4.jpg
https://s-media-cache-ak0.pinimg.com/736x/af/56/fa/af56fa5603e4e57922996dc76bc0aad3.jpg

[5] http://3.bp.blogspot.com/-d_8I7GWbTxI/Unh9urYh2TI/AAAAAAAAB2k/LRhRQ-tkhZc/s1600/Screenshot+2013-11-05+00.03.27.png

[6] http://xkcd.com/327/

----

[English](#markdown-header-regular-expressions-sql-injection) | [Español](#markdown-header-expresiones-regulares-inyeccion-de-sql)

# File System - File Recovery
##Objectives

1. Understand the implementation of the file systems inodes, data blocks, and other file system concepts by recovering deleted files using the sleuthkit tool kit.

### Pre-Module

Before working in this module the student must 

1. Have reviewed file systems concepts such as inode, file attributes, and data blocks.
2. Have available a virtual machine or a computer with the sleuthkit installed
    - http://atackpr.ccom.uprrp.edu/vms/sleuthkit.tar.gz

## Learning file system concepts by recovering a deleted file

Normally when a file is removed from the file system, the data of the file still remains in the file system data blocks until those blocks are claimed again by another file.  

Knowing how files are organized in the file system is essential to computer forensics because it allows us to recover files that are deleted or hidden inside the file system.   

As you should have learned in the course, most file systems have a hierarchical structure organized as upside down trees where the leaves of the trees are the files or empty directories, and the nodes that form the path to the leaves are directories.

![](images/fs_tree.png)

Specifically the Unix file system, is a collection of files and directories that have the following properties:

* A root (/) directory 
* Each file and directory has a name and a unique identifier, typically called the inode.
* The root directory has the inode number 2. Numbers 0 and 1 are not assigned to inodes.

The storage media is logically divided in blocks.  The blocks are divided into data blocks and the inodes. 

The data blocks contain the actual data inside the files.

There are two type of inodes:

* Directory inodes that contain the names of files and sub directories inside the directory and a pointer (inode number) to the inode of the file or directory.
    
    ![](images/directory_inode.png)

* File inodes that contain the attributes of the files, and pointers to the data blocks of the files.

    ![](images/file_inode.png)

When a user removes a file the data blocks of the file are listed as free blocks, and the inode of the file as unallocated.  Thus, if they have not been overwritten the data of the files is still there.  

To recover deleted files you can search through the unallocated inodes, and from the inode information obtain the data blocks that contain the data of the file. 

In this laboratory we will learn how to recover files using the sleuthkit[1] that have commands that let users interact with the file system inodes. We will be using an image of a Unix ext2 partition created with the dd command.  The name of the image is `recover_fs.dd`.

The main commands that will be used in this laboratory are:

* `file` - Lets you know the type of file system of the image.  Alternatively you could use `fsstat` which will provide more detailed information on the inode groups, block size, etc.

* `fls` - List file and directory names in a disk image.

    We will use fls <image> <inode>, if no inode number is given, it will use the root inode by default.   For ext2 and ext3 the root inode number would be 2.

* `ils` - List inode information

* `istat` - Display details of the inode (easier to read than ils)

* `icat` - Outputs the content of a file based on its inode number.

**Note** The images and files in the file system used in this laboratory are listed in the references.

**Note** There are other tools that facilitate the recovering of files like autopsy which is a web GUI for the sleuthkit, foremost, and Test-Disk.

### Directory listing

```
fls recover_fs.dd
```

or 

```
fls recover_fs.dd 2
```

![](images/fls_2.png)

Note the three directories and pay attention to the directories `Documents` inode 16065 and `images` 46185

```
fls recover_fs.dd 16065
```

![](images/fls_16065.png)

Note the two pdf files, one has an * the other doesn't. Also note that the inode number is 0.

```
fls recover_fs.dd 46185
```

![](images/fls_46185.png)

Note all the files with `*` (deleted), the directory `jokes` with inode 36722. 

### Getting inode information

Lets get the inode information of the deleted file in directory `Documents`, file `rooter.pdf`

Run 

```
ils recover_fs.dd -A 
```
to list all the unallocated inodes.  

![](images/ils_A.png)

Recall that the inode number of the directory `Documents` is 16065, and that the closest inode number (first field of the records) in the unallocated inodes list is 16067.

### Looking inside a file inode

Run 

```
ils recover_fs.dd 16067
```

to see the inode attributes, such as the user and group id of the file, and times of Access, Modified, and Deleted.

![](images/istat_16067.png)

and the second part of the inode `Direct blocks` and `Indirect Blocks`

![](images/istat_16067_2.png)

Note that the first direct block is found in data block 70657, and the last direct block is 70749, and that it also points to an indirect block 70679 which implies that the file is large enough to need to have and indirect block with address for other direct blocks.

### Recovering the file 

```
icat recover_fs.dd 16067 > recover1
```

Will retrieve the data from the data blocks of the file referenced by the direct blocks and indirect blocks in the inode and store it in our file system with name recover1.  

Running

```
file recover1
```

will reveal the type of file.

## Exercise 1: Recovering a file

1.  What are the time attributes for inode 46186?
2.  What are the first, and last direct blocks?
3.  Does it has indirect blocks? Which one?
4.  Recover the file in inode 46186.
5.  What is the type of the file recovered?

## Exercise 2: Recovering all deleted files

Recover all the remaining files with unallocated inodes.


## References

[1] http://www.sleuthkit.org/sleuthkit/

[2] SCIgen - An Automatic CS Paper Generator, http://pdos.csail.mit.edu/scigen/

[3] http://fc09.deviantart.net/fs70/f/2014/079/1/9/sonic\_the\_hedgehog\_2006\_pose\_by\_mintenndo-d7axbgr.png

[4] http://engineering.vanderbilt.edu/images/dept-slide-show/eecs-slide-4.jpg
https://s-media-cache-ak0.pinimg.com/736x/af/56/fa/af56fa5603e4e57922996dc76bc0aad3.jpg

[5] http://3.bp.blogspot.com/-d_8I7GWbTxI/Unh9urYh2TI/AAAAAAAAB2k/LRhRQ-tkhZc/s1600/Screenshot+2013-11-05+00.03.27.png

[6] http://xkcd.com/327/